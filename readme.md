# Download Multi files in Laravel without zip Option

Download Multi files in Laravel without zip Option
multi file download in laravel
Laravel 5 - multiple files download with response example
Laravel 6 - multiple files download with response example
Multiple files download without zip in Laravel


Project youtube tutorial link
======

>>>
Video Tutorial for this project can be found on the https://www.youtube.com/watch?v=IA03QeE59Fk.
>>>


Way
======

Project Iniatial View
======

>>>
Link: https://gitlab.com/Bons/multi-files-download-without-zip-in-laravel-view-database
>>>


======

Project Blog/Post link
======

>>>
Post/Blog for this project can be found on the http://bonstutorial.com/.
>>>


======

for contact
======

>>>
Mail: bonstutorial@gmail.com
>>>


======

in **controller**

```laravel
use DB;
public function view_multi_down(){


		$downloads=DB::table('download_info')->get();
        return view('multi_file',compact('downloads'));

    }
    
```
in **web.php**
```
<?php

use Illuminate\Http\Request;


Route::get('multi_down','DownloadController@view_multi_down');

Route::get('/postDownload',function (Request $request){

    $url=$request->urlpath;
    $pathToFile = public_path('\download\\'.$url);
    return response()->download($pathToFile);

});
```

    
 in **view**
 
 ```laravel
 <tbody>

                @foreach($downloads as $dw)
                
                <tr>
                    <td>
                        <label>
                            <input type="checkbox" class="indi_chk" name="filedata[]" value="{{$dw->file_name}}"><span>{{$dw->file_title}}</span>
                        </label>
                    </td>
                    <td class="siz_file">{{$dw->file_size}}</td>
                </tr>

                @endforeach
               
                </tbody>
```


in **jquery** 
```
<script type="text/javascript">
    var g_arr;
    $('#all_Chk').on('change',function(){

       //Return the value of a property:
       //$(selector).prop(property)
//        $(this) refers to current
       console.log($(this).prop('checked'));

//        Set the property and value:
//        $(selector).prop(property,value)
       $('.indi_chk').prop('checked',$(this).prop('checked'));

       
     enablebutton();
   });

    $('.indi_chk').on('change',function(){
       
       if(false==$(this).prop('checked')){
           $('#all_Chk').prop('checked',false);

       }


       if($('.indi_chk:checked').length==$('.indi_chk').length){
           $('#all_Chk').prop('checked',true);

       }

       enablebutton();

   });

    function enablebutton(){
        var i=1;
        $('.indi_chk:checked').each(function(){
            i++;
        });

        if(i==1){
            $('button').prop('disabled',true);

        }else{
            $('button').prop('disabled',false);

        }
    }

    //download
    var func1 = function(){
       var deferred = $.Deferred();
       window.setTimeout(function(){

           var r_path=g_arr[0];
           console.log(r_path);
           window.location.href='{!! URL::to('postDownload') !!}'+'?urlpath='+r_path;

           deferred.resolve();
       }, 1000);

       return deferred.promise();
   }
   var func2 = function(){
       var deferred = $.Deferred();
       window.setTimeout(function(){

           var r_path=g_arr[1];
           console.log(r_path);
           window.location.href='{!! URL::to('postDownload') !!}'+'?urlpath='+r_path;

           deferred.resolve();
       }, 1000);

       return deferred.promise();
   }
   var func3 = function(){
       var deferred = $.Deferred();
       window.setTimeout(function(){

           var r_path=g_arr[2];
           console.log(r_path);
           window.location.href='{!! URL::to('postDownload') !!}'+'?urlpath='+r_path;

           deferred.resolve();
       }, 1000);

       return deferred.promise();
   }


    $('.final_sub').on('click',function () {

        var i=1;
        g_arr=[];

        $('.indi_chk:checked').each(function(){
            g_arr.push($(this).val());
            i++;
        });
        // i=3
        // g_rr=['abc.pdf','fhdsh.txt']

        var j=i-1;
        if(j==1){
            func1();
           
       }else if(j==2){
          func1().then(func2);
       }else if(j==3){
          func1().then(func2).then(func3);
       }




    });


</script>
```
=============================Some Concepts================================
**jQuery promise Method**
A promise is an object that wraps an asynchronous operation and notifies when it’s done. A promise is an object that basically says, 
"This work isn't done yet, but I'll let you know when it is.“ 
A promise has its own methods which you call to tell the promise what will happen when it is successful or when it fails.
When you create a Promise, you can attach callback functions
using .then(). When the promise is resolved - that is to say, it's work is finished - all of the callbacks will be executed. 
You can also attach callbacks with .fail() or something similar, and these will be executed if the Promise is rejected
**Deferred object**
In jQuery, the Promise object is created from a Deferred object or a jQuery object. The Deferred object is a  object created by calling the jQuery.Deferred() method. 
//creating deferred object using    var deferred = $.Deferred();
** .resolve() **
it will notify that the promise is done.



Important directory in this project
======
- projectname
    - app
        - Http
            - controllers
                - DownloadController.php (controller)
    - route
        -web.php
    - resources
        - views
            - multi_file.blade.php  (view)
    -public
        -download(here dummy files are situated)
    -.env (database setup)

